#include "main.h"
#include "ConsoleUtils.h"
#include "UTF8.h"

PluginFuncs * g_functions;
uint16_t g_port;
IStaticPortMappingCollection * g_spmc;
IStaticPortMapping * g_spm;
BSTR g_bstrUDP;

char* GetIP()
{
	static char IP[16];
	PIP_ADAPTER_INFO pAdapterInfo;
	PIP_ADAPTER_INFO pAdapter = nullptr;
	DWORD dwRetVal = 0;
	ULONG ulOutBufLen = sizeof(IP_ADAPTER_INFO);

	memset(IP, 0, sizeof(IP));
	pAdapterInfo = (PIP_ADAPTER_INFO)malloc(ulOutBufLen);
	if (pAdapterInfo != nullptr)
	{
		if (GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW)
		{
			free(pAdapterInfo);
			pAdapterInfo = (PIP_ADAPTER_INFO)malloc(ulOutBufLen);
		}
		if ((dwRetVal = GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR)
		{
			pAdapter = pAdapterInfo;
			while (pAdapter)
			{
				if (pAdapter->IpAddressList.Context != 16777343) // 127.0.0.1
				{
					strcpy_s(IP, pAdapter->IpAddressList.IpAddress.String);
					break;
				}
				pAdapter = pAdapter->Next;
			}
		}
		free(pAdapterInfo);
	}
	return IP;
}

uint8_t OnServerInitialise()
{
	ServerSettings settings;
	HRESULT hr;
	IUPnPNAT * upnpnat = nullptr;
	CoInitialize(nullptr);

	g_functions->GetServerSettings(&settings);
	g_port = settings.port;

	g_bstrUDP = SysAllocString(L"UDP");

	do
	{
		hr = CoCreateInstance(CLSID_UPnPNAT, nullptr, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&upnpnat));
		if (FAILED(hr) || !upnpnat) break;

		hr = upnpnat->get_StaticPortMappingCollection(&g_spmc);
		if (FAILED(hr) || !g_spmc) break;

		BSTR bstrVCMP = SysAllocString(L"VCMP");
		BSTR bstrIPW = SysAllocString(utf8_mbstowcs(GetIP()).c_str());

		hr = g_spmc->Add(g_port, g_bstrUDP, g_port, bstrIPW, VARIANT_TRUE, bstrVCMP, &g_spm);

		SysFreeString(bstrIPW);
		SysFreeString(bstrVCMP);

		if (FAILED(hr) || !g_spm) break;

		g_spm->Release();
		upnpnat->Release();

		OutputMessage("UPnPPortMapper successful loaded! Author:ysc3839");
		return 1;
	} while (0);

	// Failed
	SysFreeString(g_bstrUDP);

	if (upnpnat != nullptr) upnpnat->Release();
	if (g_spmc != nullptr) g_spmc->Release();

	char errmsg[64];
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, nullptr, hr, MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT), errmsg, sizeof(errmsg), nullptr);
	char msg[256];
	_snprintf_s(msg, sizeof(msg), "Initilize UPnPNAT Failed! %s (HRESULT:0x%X)", errmsg, hr);
	OutputError(msg);

	CoUninitialize();

	return 0;
}

void OnServerShutdown()
{
	if (g_spmc != nullptr)
	{
		g_spmc->Remove(g_port, g_bstrUDP);
		g_spmc->Release();
	}
	SysFreeString(g_bstrUDP);
	CoUninitialize();
}

#ifdef __cplusplus
extern "C"
#endif
EXPORT unsigned int VcmpPluginInit(PluginFuncs* pluginFuncs, PluginCallbacks* pluginCalls, PluginInfo* pluginInfo)
{
	pluginInfo->pluginVersion = 0x1200; // 1.2.0.0
	strcpy_s(pluginInfo->name, "UPnPPortMapper");

	pluginInfo->apiMajorVersion = PLUGIN_API_MAJOR;
	pluginInfo->apiMinorVersion = PLUGIN_API_MINOR;

	g_functions = pluginFuncs;

	pluginCalls->OnServerInitialise = OnServerInitialise;
	pluginCalls->OnServerShutdown = OnServerShutdown;
	return 1;
}
